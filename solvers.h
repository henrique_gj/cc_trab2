#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "CommonFiles/protos.h"

// produto matriz-vetor
double* matvec(MAT* A, double* vet){
  double* prod = (double *)malloc(A->n*sizeof(double));
  int a; // indice do elemento atual em aa
  int i=-1; // indice da linha atual em ia

  for (a=0;a<A->nz;a++){
    // avanca para proximo indice em ia se indice atual de aa pertence a proxima linha da matriz
    if(i<A->n-1)
      if(a==A->IA[i+1]){
        i++;
        prod[i]=0;
      }

    // produto = matriz * vetor
    prod[i]+=A->AA[a]*vet[A->JA[a]];
  }

  return prod;
}

// soma de vetores
double* vec_plus_vec(double* vet1, double* vet2, int len){
  double* vet = (double *)malloc(len*sizeof(double));
  int i;

  for (i=0;i<len;i++)
    vet[i]+=vet1[i]+vet2[i];

  return vet;
}

// produto vetorial
double vecvec(double* vet1, double* vet2, int len) {
  int i;
  double soma = 0;
  for(i=0; i<len; i++) {
    soma += vet1[i]*vet2[i];
  }
  return soma;
}

// norma euclidiana
double norm_2(double* vet, int len) {
  return sqrt(vecvec(vet,vet,len));
}

// norma infinita
double norm_inf(double* vet, int len) {
  int i;
  double maior=0;
  for(i=0;i<len;i++)
    if(fabs(vet[i])>maior)
      maior=fabs(vet[i]);
  return maior;
}

// produto vetor-escalar
double* scavec(double escalar, double* vet, int len) {
  int i;
  double* vet2 = (double *)malloc(len * sizeof(double));
  for(i=0;i<len;i++)
    vet2[i]=escalar*vet[i];
  return vet2;
}

// retorna uma cópia de um array
double* copia_array(double* vet1, int len){
  int i;
  double* vet2 = (double *)malloc(len * sizeof(double));
  for(i=0;i<len;i++)
    vet2[i]=vet1[i];
  return vet2;
}

/*  Copia uma array para dentro de uma matriz
    Dada uma matriz mat, um parametro pos e um parametro len,
    mat[pos][i] <- vet[i] para todo i | 0<=i<len
*/
void copia_array_para_matriz(double* vet, int pos, int len, double** mat){
  int i;
  for (i=0;i<len;i++)
    mat[pos][i] = vet[i];
}

double* residuo(MAT* A, double* b, double* x){
  double* r = (double *)malloc(A->n*sizeof(double));
  int a; // indice do elemento atual em aa
  int i=-1; // indice da linha atual em ia

  for (a=0;a<A->nz;a++){
    // avanca para proximo indice em ia se indice atual de aa pertence a proxima linha da matriz
    // inicia r = b para subtrair A*x depois
    if(i<A->n-1)
      if(a==A->IA[i+1]) {
        i++;
        r[i]=b[i];
      }

    r[i]-=A->AA[a]*x[A->JA[a]];
  }

  return r;
}

double* gmres(MAT* A, double* b, int lmax, double tol, int k, int *l, FILE* out){

  // se k = 0, nao usa restart
  if(k==0)
    k=A->n;

  // declaracoes
  int i, j, t;
  double ji;
  double** u = (double **)malloc((k+1)*sizeof(double*));
  for(i=0;i<=k;i++)
    u[i] = (double *)malloc(A->n*sizeof(double));
  double* e = (double *)malloc(k * sizeof(double));
  double* x = (double *)calloc(A->n, sizeof(double));
  double** h = (double **)malloc(k*sizeof(double*));
  for(i=0;i<k;i++)
    h[i] = (double *)malloc((k+1)*sizeof(double));
  double p;
  double* c = (double *)calloc(k, sizeof(double));
  double* s = (double *)calloc(k, sizeof(double));
  double* r = (double *)calloc(k, sizeof(double));
  double* y = (double *)calloc(k, sizeof(double));
  double soma_hy;
  
  double* aux0;
  double * res;
  double norma_r;

  // inicializa x0
  //for(i=0;i<A->n;i++)
    //x[i] = 0;

  // inicializa c e s
  //for(i=0;i<k;i++)
    //c[i] = s[i] = r[i] = y[i] = 0;

  //e = tol||b||2
  double _e = tol*norm_2(b,A->n);
  *l = 0;

  do { // repeat

    i = 0;

    // u[i] = b - Ax
    aux0 = residuo(A,b,x);
    for(t=0; t<A->n; t++)
      u[i][t] = aux0[t];
    /*copia_array_para_matriz(
      residuo(A,b,x),
      i,A->n,u);*/

    // e[i] = ||u[i]||2 = p
    e[i] = p = norm_2(&u[i][0],A->n);

    // u[i]=r/e[i]
    for(t=0; t<A->n; t++)
      u[i][t] = aux0[t]/e[i];
    //copia_array_para_matriz(scavec(1/e[i],residuo(A,b,x),A->n), i, A->n, u);
    free(aux0);

    do{

      // Gram-Schmidt

      aux0 = matvec(A,&u[i][0]);
      copia_array_para_matriz(aux0, i+1, A->n, u);
      free(aux0);
      
      for(j=0;j<=i;j++){

        h[i][j] = vecvec(&u[i+1][0],&u[j][0],A->n);

        for(t=0; t<A->n; t++)
          u[i+1][t] = u[i+1][t] - h[i][j]*u[j][t];
        /*copia_array_para_matriz(
          vec_plus_vec(&u[i+1][0],scavec(-1*h[i][j],&u[j][0],A->n),A->n),
          i+1, A->n, u);*/
      }

      h[i][i+1]=norm_2(&u[i+1][0],A->n);

      for(t=0; t<A->n; t++)
        u[i+1][t] = u[i+1][t]/h[i][i+1];
      /*copia_array_para_matriz(
        scavec(1/h[i][i+1],&u[i+1][0],A->n)
        ,i+1,A->n,u);*/

      // Algoritmo QR
      for(j=0;j<i;j++){
        ji = h[i][j];
        h[i][j] = c[j]*ji + s[j]*h[i][j+1];
        h[i][j+1] = -s[j]*ji + c[j]*h[i][j+1];
      }
    
      r[i] = sqrt(h[i][i]*h[i][i] + h[i][i+1]*h[i][i+1]);
      c[i] = h[i][i]/r[i];
      s[i] = h[i][i+1]/r[i];
      h[i][i] = r[i];
      h[i][i+1] = 0;
      e[i+1] = -s[i]*e[i];
      e[i] = c[i]*e[i];
      p = fabs(e[i+1]);
      // i = i + 1
      i++;
    }while((p>_e) && (i<k));
    i = i - 1;
	  y[i] = e[i]/h[i][i];
	  for (t=i-1;t>=0;t--)
	  {
	  	soma_hy = 0;
	  	for (j=t+1;j<=i;j++)
	  		soma_hy += h[j][t]*y[j];	
	  	y[t] = (1./h[t][t])*(e[t]-soma_hy);
	  }	
    // x = sum(y[j]u[j]) | 1<=j<=i
    for(j=0;j<=i;j++){
      //copia_array_para_matriz(
      for(t=0; t<A->n; t++)
        x[t] += u[j][t]*y[j];
      //x = vec_plus_vec(scavec(y[j],&u[j][0],A->n),x,A->n);
        //,j, k, A->n, u);
    }
    (*l)++;
  } while((p>=_e) && (*l<lmax)); // until
  res = residuo(A,b,x);
  norma_r = norm_inf(res,A->n);
  free(res);
  printf("Residuo: %f\n", norma_r);
  
  //free(e);
  free(c);
  free(s);
  free(r);
  free(y);
  for(i=0;i<=k;i++)
    free(u[i]);
  //free(u);
  for(i=0;i<k;i++)
    free(h[i]);
  //free(h);
  
  return x;
}
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "diferencas.h"
#include "solvers.h"
#include "CommonFiles/heads.h"
#include "CommonFiles/protos.h"

double betax1(int x, int y, double hx, double hy) {
	return 0;
}

double betay1(int x, int y, double hx, double hy) {
	return 0;
}

double betax2(int x, int y, double hx, double hy) {
	return 1;
}

double betay2(int x, int y, double hx, double hy) {
	return 20*y*hy;
}

double f1(int x, int y, double hx, double hy) {
	return 0;
}

double avalia_erro_teste2(double *xx, int n, double a, double b, double hx, double hy) {
	double maior = 0, novo, x, y;
	int i, w;
	w	= (int)(b-a)/hx + 1.5;
	for (i = 0; i < n; i++) {
		x = (i%w)*hx;
		y = (i/w)*hy;
		novo = 10*x*y*(1-x)*(1-y)*pow(M_E,pow(x,4.5));
		//printf("i %d \t x %f \t y %f \t Exato: %f \t Aprox: %f \n", i,x,y, novo, xx[i]);
		novo = fabs(novo - xx[i]);
		if (novo > maior) maior = novo;
	}
	//printf("w %d\n",w);
	return maior;
}

double f2(double x, double y, double hx, double hy) {
	x *= hx; y*=hy;
	int k = 1, B = 1, C = 20*y, G = 1;
	
    return 20*y*(10*(1-x)*x*exp(pow(x,4.5))*(1-y)-10*(1-x)*x*exp(pow(x,4.5))*y) -202.5*(1-x)*pow(x,8.0)*exp(pow(x,4.5))*(1-y)*y+45.0*(1-x)*pow(x,4.5)*exp(pow(x,4.5))*(1-y)*y +90.0*pow(x,4.5)*exp(pow(x,4.5))*(1-y)*y -247.5*(1-x)*pow(x,3.5)*exp(pow(x,4.5))*(1-y)*y +10*(1-x)*x*exp(pow(x,4.5))*(1-y)*y -10*x*exp(pow(x,4.5))*(1-y)*y +10*(1-x)*exp(pow(x,4.5))*(1-y)*y +20*exp(pow(x,4.5))*(1-y)*y +20*(1-x)*x*exp(pow(x,4.5));	
	
	/*return -k*(202.5*pow(M_E,pow(x,4.5))*(1-x)*pow(x,8)*(1-y)*y
			-90*pow(M_E,pow(x,4.5))*pow(x,4.5)*(1-y)*y
			+247.5*pow(M_E,pow(x,4.5))*(1-x)*pow(x,3.5)*(1-y)*y
			-20*pow(M_E,pow(x,4.5))*(1-y)*y
			-20*pow(M_E,pow(x,4.5))*(1-x)*x)
		+B*(45*pow(M_E,pow(x,4.5))*(1-x)*pow(x,4.5)*(1-y)*y
			-10*pow(M_E,pow(x,4.5))*x*(1-y)*y
			+10*pow(M_E,pow(x,4.5))*(1-x)*(1-y)*y)
		+C*(10*pow(M_E,pow(x,4.5))*(1-x)*x*(1-y)
			-10*pow(M_E,pow(x,4.5))*(1-x)*x*y)
		+10*G*pow(M_E,pow(x,4.5))*(1-x)*x*(1-y)*y;*/
}

double f3(double x, double y, double hx, double hy) {
	return 70;
}

void main() {
	
	FILE* graph = fopen("graph.txt", "w");
	
	FILE* arq = fopen("saida.txt", "w");
	
	clock_t start, end;
    double cpu_time_used;
	
	int i,l;
	
	double* f;
	
	PVC* pvc1 = (PVC*)malloc(sizeof(PVC));
	pvc1->tipo	= 1;
	pvc1->u		= 10;
	
	PVC* pvc2 = (PVC*)malloc(sizeof(PVC));
	pvc2->tipo	= 1;
	pvc2->u		= 0;
	
	PVC* pvc3_b = (PVC*)malloc(sizeof(PVC));
	pvc3_b->tipo	= 1;
	pvc3_b->u		= 200;
	
	PVC* pvc3_c = (PVC*)malloc(sizeof(PVC));
	pvc3_c->tipo	= 3;
	pvc3_c->alfa	= 1;
	pvc3_c->beta	= 1;
	pvc3_c->gamma	= 70;
	
	PVC* pvc3_d = (PVC*)malloc(sizeof(PVC));
	pvc3_d->tipo	= 1;
	pvc3_d->u		= 70;
	
	PVC* pvc3_e = (PVC*)malloc(sizeof(PVC));
	pvc3_e->tipo	= 1;
	pvc3_e->u		= 70;
	
	int _n = 50, _m = 50;
	double hx = 1.0/(_n-1);
	double hy = 1.0/(_m-1);
	
	printf("hx %f hy %f\n", hx, hy);
	
    start = clock();
	
	// teste 1
	//MAT* m = transp2d(0,1,0,1,0.2,0.2,1,&betax1,&betay1,0,pvc1,pvc1,pvc1,pvc1,&f,&f1);
	// TESTE 2
	//MAT* m = transp2d(0,1,0,1,hx,hy,1,&betax2,&betay2,1,pvc2,pvc2,pvc2,pvc2,&f,&f2);
	
	// TESTE 3
	MAT* m = transp2d(0,1,0,1,hx,hy,1,&betax2,&betax2,0,pvc3_d,pvc3_e,pvc3_b,pvc3_c,&f,&f3);
	
	// TESTE 3 SEM CONDICAO MISTA
	//MAT* m = transp2d(0,1,0,1,hx,hy,1,&betax2,&betax2,0,pvc3_b,pvc3_d,pvc3_d,pvc3_e,&f,&f3);
	
	double* x = gmres(m, f, /*lmax*/1000, /*tol*/0.000001, /*k*/500, &l, arq);
	
	end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	
	for(i=0; i<m->n;i++)
		fprintf(graph, "%.10f %.10f %.10f\n", (i%_n)*hx,(i/_n)*hy,x[i]);
	
	printf("Iteracoes: %d\n",l);
	printf("Matriz ordem: %d\n",m->n);
	//printf("O erro foi de %f\n",avalia_erro_teste2(x, m->n, 0, 1,hx,hy));
	//printf("A diferenca foi de %f\n",avalia_diferenca_teste3(x, x_, _m*_n));
	printf("Tempo = %f\n", cpu_time_used);
	
	/*for(i=0;i<m->nz;i++) printf("AA[%d] = %f\n",i,m->AA[i]);
	printf("===== teste =======\n");
	for(i=0;i<m->nz;i++) printf("JA[%d] = %d\n",i,m->JA[i]);
	printf("===== teste =======\n");
	for(i=0;i<m->n;i++) printf("IA[%d] = %d\n",i,m->IA[i]);*/
	/*printf("===== teste =======\n");
	for(i=0;i<m->n;i++) printf("f[%d] = %d\n",i,f[i]);*/
	
	fclose(graph);
}
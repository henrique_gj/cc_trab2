#include <stdio.h>
#include <stdlib.h>
#include "CommonFiles/heads.h"

typedef struct {
	int tipo; // 1 = valor; 2 = fluxo; 3 = misto
	double u, sigma, alfa, beta, gamma;
	// u = valor prescrito
	// sigma = fluxo prescrito
	// alfa, beta, gama = misto
} PVC;

// USO:
// a, b, c e d representam os limites do domínio (a,b)x(c,d)
// hx e hy representam a grossura da malha horizontal e verticalmente
// k e o coeficiente que multiplica as derivadas segundas
// bx e by sao funcoes que calculam beta_x e beta_y em funcao de (x,y)
// gama e o coeficiente somado no final do lado esquerdo da equacao
// pvc_b, pvc_c, pvc_d e pvc_e sao as condicoes de contorno a esquerda, direita, acima e abaixo, respectivamente
// f e um ponteiro para o vetor de termos independentes (sera inicializado pela tramp2d). devera ser usado no solver
// gera_f e uma funcao que calcula o i-esimo componente do vetor de termos independentes, desconsiderando-se as condicoes de contorno.
MAT* transp2d(double a, double b, double c, double d, double hx, double hy, double k, double(bx)(int x, int y, double hx, double hy), double(by)(int x, int y, double hx, double hy), double gama, PVC* pvc_b, PVC* pvc_c, PVC* pvc_d, PVC* pvc_e, double** f, double(gera_f)(double x, double y, double hx, double hy)){
	
	MAT* m;
	int i, j, width, height, n, nz, x, y;
	double* AA;
	double* D;
	int* JA;
	int* IA;
	
	// tamanho horizontal da malha
	width	= (int)(b-a)/hx + 1.5;
	// tamanho vertical da malha
	height	= (int)(d-c)/hy + 1.5;
	// ordem da matriz
	n	= (width)*(height);
	// numero de elementos nao nulos (antes da aplicacao das condicoes de contorno)
	nz	= 5*n-2*width-2;
	
	(*f) = (double*)calloc(n,sizeof(double));
	
	m	= (MAT*)malloc(sizeof(MAT));
	
	AA	= (double *) calloc(nz, sizeof (double));
	D	= (double *) calloc(n,  sizeof (double));
	JA	= (int    *) calloc(nz, sizeof (int));
	IA	= (int    *) calloc(n+1,sizeof (int));
	
	// coeficientes para aproximacoes de segunda ordem
	//double _a = gama + 2*k*(1/(hx*hx) + 1/(hy*hy));
	//double _b = -k/(hx*hx) - bx/(2*hx);
	//double _c = -k/(hx*hx) + bx/(2*hx);
	//double _d = -k/(hy*hy) - by/(2*hy);
	//double _e = -k/(hy*hy) + by/(2*hy);
	
	// i armazena a posicao onde os valores serao inseridos nos arrays AA e JA
	i = 0;
	// j representa a linha da matriz que estamos analisando
	for(j=0; j < n; j++) {
		
		x = j%width;
		y = j/width;
		
		IA[j] = i;
		
		// se d nao cai fora da matriz
		if(j-width >= 0) {
			// condicao de valor prescrito
			if ( ((x == 0) && (pvc_b->tipo == 1)) || ((x == width-1) && (pvc_c->tipo == 1)) || ((y == 0) && (pvc_d->tipo == 1)) ||  ((y == height-1) && (pvc_e->tipo == 1)))
				AA[i] = 0;
			else
				AA[i] = -k/(hy*hy) - by(x,y,hx,hy)/(2.0*hy); //_d;
			JA[i] = j-width;
			i++;
		}
		// se b nao cai fora da matriz
		if(j > 0) {
			// condicao de valor prescrito
			if ( ((x == 0) && (pvc_b->tipo == 1)) || ((x == width-1) && (pvc_c->tipo == 1)) || ((y == 0) && (pvc_d->tipo == 1)) ||  ((y == height-1) && (pvc_e->tipo == 1)))
				AA[i] = 0;
			else
				AA[i] = -k/(hx*hx) - bx(x,y,hx,hy)/(2.0*hx); //_b;
			JA[i] = j-1;
			i++;
		}
		
		// a nunca cai fora da matriz
		// condicao de valor prescrito
		if ((x == 0) && (pvc_b->tipo == 1)) {
			AA[i] = 1;
			(*f)[j] = pvc_b->u;
		}
		else
		if ((x == width-1) && (pvc_c->tipo == 1)) {
			AA[i] = 1;
			(*f)[j] = pvc_c->u;
		}
		else
		if ((y == 0) && (pvc_d->tipo == 1)) {
			AA[i] = 1;
			(*f)[j] = pvc_d->u;
		}
		else
		if ((y == height-1) && (pvc_e->tipo == 1)) {
			AA[i] = 1;
			(*f)[j] = pvc_e->u;
		}
		else
		// condicao de fluxo prescrito
		if ((x == 0) && (pvc_b->tipo == 2)) {
			AA[i] = D[j] = gama + 2*k*(1/(hx*hx) + 1/(hy*hy)) + (-k/(hx*hx) - bx(x,y,hx,hy)/(2*hx));
			(*f)[j] = gera_f(x,y,hx,hy) + (-k/(hx*hx) - bx(x,y,hx,hy)/(2*hx))*hx*pvc_b->sigma/k;
		}
		else
		if ((x == width-1) && (pvc_c->tipo == 2)) {
			AA[i] = D[j] = gama + 2*k*(1/(hx*hx) + 1/(hy*hy)) + (-k/(hx*hx) + bx(x,y,hx,hy)/(2*hx));
			(*f)[j] = gera_f(x,y,hx,hy) + (-k/(hx*hx) + bx(x,y,hx,hy)/(2*hx))*hx*pvc_c->sigma/k;
		}
		else
		if ((y == 0) && (pvc_d->tipo == 2)) {
			AA[i] = D[j] = gama + 2*k*(1/(hx*hx) + 1/(hy*hy)) + (-k/(hy*hy) - by(x,y,hx,hy)/(2*hy));
			(*f)[j] = gera_f(x,y,hx,hy) + (-k/(hy*hy) - by(x,y,hx,hy)/(2*hy))*hy*pvc_d->sigma/k;
		}
		else
		if ((y == height-1) && (pvc_e->tipo == 2)) {
			AA[i] = D[j] = gama + 2*k*(1/(hx*hx) + 1/(hy*hy)) + (-k/(hy*hy) + by(x,y,hx,hy)/(2*hy));
			(*f)[j] = gera_f(x,y,hx,hy) + (-k/(hy*hy) + by(x,y,hx,hy)/(2*hy))*hy*pvc_e->sigma/k;
		}
		else
		// condicao mista
		if ((x == 0) && (pvc_b->tipo == 3)) {
			AA[i] = D[j] = gama + 2.0*k*(1.0/(hx*hx) + 1.0/(hy*hy)) + (-k/(hx*hx) - bx(x,y,hx,hy)/(2.0*hx))*(1.0 - hx*pvc_b->beta/pvc_b->alfa);
			(*f)[j] = gera_f(x,y,hx,hy) - (-k/(hx*hx) - bx(x,y,hx,hy)/(2.0*hx))*hx*pvc_b->gamma/pvc_b->alfa;
			}
		else
		if ((x == width-1) && (pvc_c->tipo == 3)) {
			AA[i] = D[j] = gama + 2.0*k*(1.0/(hx*hx) + 1.0/(hy*hy)) + (-k/(hx*hx) + bx(x,y,hx,hy)/(2.0*hx))*(1.0 - hx*pvc_c->beta/pvc_c->alfa);
			(*f)[j] = gera_f(x,y,hx,hy) - (-k/(hx*hx) + bx(x,y,hx,hy)/(2.0*hx))*hx*pvc_c->gamma/pvc_c->alfa;
			//printf("A = %f \t f = %f \t a = %f c() = %f\n",AA[i],(*f)[j],gama + 2.0*k*(1.0/(hx*hx) + 1.0/(hy*hy)),(-k/(hx*hx) + bx(x,y,hx,hy)/(2.0*hx))*(1.0 - hx*pvc_c->beta/pvc_c->alfa));
		}
		else
		if ((y == 0) && (pvc_d->tipo == 3)) {
			AA[i] = D[j] = gama + 2.0*k*(1.0/(hx*hx) + 1.0/(hy*hy)) + (-k/(hy*hy) - by(x,y,hx,hy)/(2.0*hy))*(1.0 - hy*pvc_d->beta/pvc_d->alfa);
			(*f)[j] = gera_f(x,y,hx,hy) - (-k/(hy*hy) - by(x,y,hx,hy)/(2.0*hy))*hy*pvc_d->gamma/pvc_d->alfa;
		}
		else
		if ((y == height-1) && (pvc_e->tipo == 3)) {
			AA[i] = D[j] = gama + 2.0*k*(1.0/(hx*hx) + 1.0/(hy*hy)) + (-k/(hy*hy) + by(x,y,hx,hy)/(2.0*hy))*(1.0 - hy*pvc_e->beta/pvc_e->alfa);
			(*f)[j] = gera_f(x,y,hx,hy) - (-k/(hy*hy) + by(x,y,hx,hy)/(2.0*hy))*hy*pvc_e->gamma/pvc_e->alfa;
		}
		else {
			AA[i] = D[j] = gama + 2.0*k*(1.0/(hx*hx) + 1.0/(hy*hy)); //_a;
			(*f)[j] = gera_f(x,y,hx,hy);
			//printf("geta_f(%d) %f \n",j,gera_f(x,y,hx,hy));
		}
		JA[i] = j;
		i++;
		
		// se c nao cai fora da matriz
		if(j + 1 < n) {
			// condicao de valor prescrito
			if ( ((x == 0) && (pvc_b->tipo == 1)) || ((x == width-1) && (pvc_c->tipo == 1)) || ((y == 0) && (pvc_d->tipo == 1)) ||  ((y == height-1) && (pvc_e->tipo == 1)))
				AA[i] = 0;
			else
				AA[i] = -k/(hx*hx) + bx(x,y,hx,hy)/(2.0*hx); //_c;
			JA[i] = j+1;
			i++;
		}
		// se e nao cai fora da matriz
		if(j+width < n) {
			// condicao de valor prescrito
			if ( ((x == 0) && (pvc_b->tipo == 1)) || ((x == width-1) && (pvc_c->tipo == 1)) || ((y == 0) && (pvc_d->tipo == 1)) ||  ((y == height-1) && (pvc_e->tipo == 1)))
				AA[i] = 0;
			else
				AA[i] = -k/(hy*hy) + by(x,y,hx,hy)/(2.0*hy); //_e;
			JA[i] = j+width;
			i++;
		}
	}
	
	// ultimo elemento de IA e sempre o numero total de elementos nao nulos
	IA[n]	= nz;
	
	
	m->m	= m->n	= n;
	m->nz	= nz;
	m->AA	= AA;
	m->D	= D;
	m->JA	= JA;
	m->IA	= IA;
	
	return m;
}

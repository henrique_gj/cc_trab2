#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "solvers.h"

double _a, _b, _c, _d, hx, hy, _k, gama;
double(*bx)(int x, int y, double hx, double hy);
double(*by)(int x, int y, double hx, double hy);
PVC* pvc_b, *pvc_c, *pvc_d, *pvc_e;
double(*gera_f)(int x, int y, double hx, double hy);

double betax1(int x, int y, double hx, double hy) {
	return 0;
}

double betay1(int x, int y, double hx, double hy) {
	return 0;
}

double betax2(int x, int y, double hx, double hy) {
	return 1;
}

double betay2(int x, int y, double hx, double hy) {
	return 20*y*hy;
}

double f1(int x, int y, double hx, double hy) {
	return 0;
}

double avalia_erro_teste2(double *xx, int n, double a, double b, double hx, double hy) {
	double maior = 0, novo, x, y;
	int i, w;
	w	= (int)(b-a)/hx + 1.5;
	for (i = 0; i < n; i++) {
		x = (i%w)*hx;
		y = (i/w)*hy;
		novo = 10*x*y*(1-x)*(1-y)*pow(M_E,pow(x,4.5));
		//printf("i %d \t x %f \t y %f \t Exato: %f \t Aprox: %f \n", i,x,y, novo, xx[i]);
		novo = fabs(novo - xx[i]);
		if (novo > maior) maior = novo;
	}
	//printf("w %d\n",w);
	return maior;
}

double f2(int x_, int y_, double hx, double hy) {
	double x = x_*hx; double y = y_*hy;
	int k = 1, B = 1, C = 20*y, G = 1;
	
    return 20*y*(10*(1-x)*x*exp(pow(x,4.5))*(1-y)-10*(1-x)*x*exp(pow(x,4.5))*y) -202.5*(1-x)*pow(x,8.0)*exp(pow(x,4.5))*(1-y)*y+45.0*(1-x)*pow(x,4.5)*exp(pow(x,4.5))*(1-y)*y +90.0*pow(x,4.5)*exp(pow(x,4.5))*(1-y)*y -247.5*(1-x)*pow(x,3.5)*exp(pow(x,4.5))*(1-y)*y +10*(1-x)*x*exp(pow(x,4.5))*(1-y)*y -10*x*exp(pow(x,4.5))*(1-y)*y +10*(1-x)*exp(pow(x,4.5))*(1-y)*y +20*exp(pow(x,4.5))*(1-y)*y +20*(1-x)*x*exp(pow(x,4.5));	
}

double f3(int x, int y, double hx, double hy) {
	return 70;
}

void main() {
	
	FILE* graph = fopen("graph.txt", "w");
	
	FILE* arq = fopen("saida.txt", "w");
	
	clock_t start, end;
    double cpu_time_used;
	
	int l, i;
	
	double* f;
	
	PVC* pvc1 = (PVC*)malloc(sizeof(PVC));
	pvc1->tipo	= 1;
	pvc1->u		= 15;
	
	PVC* pvc2 = (PVC*)malloc(sizeof(PVC));
	pvc2->tipo	= 1;
	pvc2->u		= 0;
	
	PVC* pvc3_b = (PVC*)malloc(sizeof(PVC));
	pvc3_b->tipo	= 1;
	pvc3_b->u		= 200;
	
	PVC* pvc3_c = (PVC*)malloc(sizeof(PVC));
	pvc3_c->tipo	= 3;
	pvc3_c->alfa	= 1;
	pvc3_c->beta	= 1;
	pvc3_c->gamma	= 70;
	
	PVC* pvc3_d = (PVC*)malloc(sizeof(PVC));
	pvc3_d->tipo	= 1;
	pvc3_d->u		= 70;
	
	PVC* pvc3_e = (PVC*)malloc(sizeof(PVC));
	pvc3_e->tipo	= 1;
	pvc3_e->u		= 70;
	
	int _m = 50; int _n = 50;
	hx = 1.0/(_m-1); hy = 1.0/(_n-1);
	
	// teste 1
	//MAT* m = transp2d(0,1,0,1,0.2,0.2,1,&betax1,&betay1,0,pvc1,pvc1,pvc1,pvc1,&f,&f1);
	// TESTE 2
	//MAT* m = transp2d(0,1,0,1,0.00200400801,0.001001001,1,&betax2,&betay2,1,pvc2,pvc2,pvc2,pvc2,&f,&f2);
	// TESTE 3
	//MAT* m = transp2d(0,1,0,1,0.00200400801,0.001001001,1,&betax1,&betay1,1,pvc3_b,pvc3_c,pvc3_d,pvc3_e,&f,&f3);

	/*// parâmetros do teste 1
	_a = 0; _b = 1; _c = 0; _d = 1; _k = 1; gama = 0;
	bx = &betax1; by = &betay1;
	pvc_b = pvc1; pvc_c = pvc1; pvc_d = pvc1; pvc_e = pvc1;
	gera_f = &f1;*/
	
	/*// parâmetros do teste 2
	_a = 0; _b = 1; _c = 0; _d = 1; _k = 1; gama = 1;
	bx = &betax2; by = &betay2;
	pvc_b = pvc2; pvc_c = pvc2; pvc_d = pvc2; pvc_e = pvc2;
	gera_f = &f2;*/
	
	// parâmetros do teste 3
	_a = 0; _b = 1; _c = 0; _d = 1; _k = 1; gama = 0;
	bx = &betax2; by = &betax2;
	pvc_b = pvc3_d; pvc_c = pvc3_e; pvc_d = pvc3_b; pvc_e = pvc3_c;
	gera_f = &f3;
	
    start = clock();
	double* x = gmres(/*lmax*/1000, /*tol*/0.000001, /*k*/500, &l, arq);
	//printf("==== q1\n");
	end = clock();
	//printf("==== q2\n");
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	//printf("==== q3\n");
	
	for(i=0; i<_m*_n;i++)
		fprintf(graph, "%.10f %.10f %.10f\n", (i%_n)*hx,(i/_n)*hy,x[i]);
	
	//for(i = 0; i < 36; i++) printf("x[%d] = %f\n", i, x[i]);
	
	printf("Iteracoes: %d\n",l);
	printf("Matriz ordem: %d\n",_m*_n);
	//printf("O erro foi de %f\n",avalia_erro_teste2(x, _m*_n, 0, 1, hx,hy));
	printf("Tempo = %f\n", cpu_time_used);
	
	fclose(graph);
}
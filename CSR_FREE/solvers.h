#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

typedef struct {
	int tipo; // 1 = valor; 2 = fluxo; 3 = misto
	double u, sigma, alfa, beta, gamma;
	// u = valor prescrito
	// sigma = fluxo prescrito
	// alfa, beta, gama = misto
} PVC;

extern double _a, _b, _c, _d, hx, hy, _k, gama;
extern double(*bx)(int x, int y, double hx, double hy);
extern double(*by)(int x, int y, double hx, double hy);
extern PVC* pvc_b, *pvc_c, *pvc_d, *pvc_e;
extern double(*gera_f)(int x, int y, double hx, double hy);

// produto matriz-vetor
double* matvec(int n, double* vet){
	double* prod = (double *)calloc(n,sizeof(double));
	int j, width, height, x, y;
	
	// tamanho horizontal da malha
	width	= (int)(_b-_a)/hx + 1.5;
	// tamanho vertical da malha
	height	= (int)(_d-_c)/hy + 1.5;

	for(j=0; j < n; j++) {
		
		x = j%width;
		y = j/width;
		
		// se d nao cai fora da matriz
		if(j-width >= 0) {
			// condicao de valor prescrito
			if ( ((x == 0) && (pvc_b->tipo == 1)) || ((x == width-1) && (pvc_c->tipo == 1)) || ((y == 0) && (pvc_d->tipo == 1)) ||  ((y == height-1) && (pvc_e->tipo == 1)))
				{}
			else
				prod[j] += (-_k/(hy*hy) - by(x,y,hx,hy)/(2.0*hy))*vet[j-width]; //_d;
		}
		// se b nao cai fora da matriz
		if(j > 0) {
			// condicao de valor prescrito
			if ( ((x == 0) && (pvc_b->tipo == 1)) || ((x == width-1) && (pvc_c->tipo == 1)) || ((y == 0) && (pvc_d->tipo == 1)) ||  ((y == height-1) && (pvc_e->tipo == 1)))
				{}
			else
				prod[j] += (-_k/(hx*hx) - bx(x,y,hx,hy)/(2.0*hx))*vet[j-1]; //_b;
		}
		
		// a nunca cai fora da matriz
		// condicao de valor prescrito
		if ((x == 0) && (pvc_b->tipo == 1)) {
			prod[j] = vet[j];
		}
		else
		if ((x == width-1) && (pvc_c->tipo == 1)) {
			prod[j] = vet[j];
		}
		else
		if ((y == 0) && (pvc_d->tipo == 1)) {
			prod[j] = vet[j];
		}
		else
		if ((y == height-1) && (pvc_e->tipo == 1)) {
			prod[j] = vet[j];
		}
		else
		// condicao de fluxo prescrito
		if ((x == 0) && (pvc_b->tipo == 2)) {
			prod[j] += (gama + 2*_k*(1/(hx*hx) + 1/(hy*hy)) + (-_k/(hx*hx) - bx(x,y,hx,hy)/(2*hx)))*vet[j];
		}
		else
		if ((x == width-1) && (pvc_c->tipo == 2)) {
			prod[j] += (gama + 2*_k*(1/(hx*hx) + 1/(hy*hy)) + (-_k/(hx*hx) + bx(x,y,hx,hy)/(2*hx)))*vet[j];
		}
		else
		if ((y == 0) && (pvc_d->tipo == 2)) {
			prod[j] += (gama + 2*_k*(1/(hx*hx) + 1/(hy*hy)) + (-_k/(hy*hy) - by(x,y,hx,hy)/(2*hy)))*vet[j];
		}
		else
		if ((y == height-1) && (pvc_e->tipo == 2)) {
			prod[j] += (gama + 2*_k*(1/(hx*hx) + 1/(hy*hy)) + (-_k/(hy*hy) + by(x,y,hx,hy)/(2*hy)))*vet[j];
		}
		else
		// condicao mista
		if ((x == 0) && (pvc_b->tipo == 3)) {
			prod[j] += (gama + 2.0*_k*(1.0/(hx*hx) + 1.0/(hy*hy)) + (-_k/(hx*hx) - bx(x,y,hx,hy)/(2.0*hx))*(1.0 - hx*pvc_b->beta/pvc_b->alfa))*vet[j];
		}
		else
		if ((x == width-1) && (pvc_c->tipo == 3)) {
			prod[j] += (gama + 2.0*_k*(1.0/(hx*hx) + 1.0/(hy*hy)) + (-_k/(hx*hx) + bx(x,y,hx,hy)/(2.0*hx))*(1.0 - hx*pvc_c->beta/pvc_c->alfa))*vet[j];
		}
		else
		if ((y == 0) && (pvc_d->tipo == 3)) {
			prod[j] += (gama + 2.0*_k*(1.0/(hx*hx) + 1.0/(hy*hy)) + (-_k/(hy*hy) - by(x,y,hx,hy)/(2.0*hy))*(1.0 - hy*pvc_d->beta/pvc_d->alfa))*vet[j];
		}
		else
		if ((y == height-1) && (pvc_e->tipo == 3)) {
			prod[j] += (gama + 2.0*_k*(1.0/(hx*hx) + 1.0/(hy*hy)) + (-_k/(hy*hy) + by(x,y,hx,hy)/(2.0*hy))*(1.0 - hy*pvc_e->beta/pvc_e->alfa))*vet[j];
		}
		else {
			prod[j] += (gama + 2.0*_k*(1.0/(hx*hx) + 1.0/(hy*hy)))*vet[j]; //_a;
		}
		
		// se c nao cai fora da matriz
		if(j + 1 < n) {
			// condicao de valor prescrito
			if ( ((x == 0) && (pvc_b->tipo == 1)) || ((x == width-1) && (pvc_c->tipo == 1)) || ((y == 0) && (pvc_d->tipo == 1)) ||  ((y == height-1) && (pvc_e->tipo == 1)))
				{}
			else
				prod[j] += (-_k/(hx*hx) + bx(x,y,hx,hy)/(2.0*hx))*vet[j+1]; //_c;~
		}
		// se e nao cai fora da matriz
		if(j+width < n) {
			// condicao de valor prescrito
			if ( ((x == 0) && (pvc_b->tipo == 1)) || ((x == width-1) && (pvc_c->tipo == 1)) || ((y == 0) && (pvc_d->tipo == 1)) ||  ((y == height-1) && (pvc_e->tipo == 1)))
				{}
			else
				prod[j] += (-_k/(hy*hy) + by(x,y,hx,hy)/(2.0*hy))*vet[j+width]; //_e;
		}
	}

	return prod;
}

// soma de vetores
double* vec_plus_vec(double* vet1, double* vet2, int len){
  double* vet = (double *)malloc(len*sizeof(double));
  int i;

  for (i=0;i<len;i++)
    vet[i]+=vet1[i]+vet2[i];

  return vet;
}

// produto vetorial
double vecvec(double* vet1, double* vet2, int len) {
  int i;
  double soma = 0;
  for(i=0; i<len; i++) {
    soma += vet1[i]*vet2[i];
  }
  return soma;
}

// norma euclidiana
double norm_2(double* vet, int len) {
  return sqrt(vecvec(vet,vet,len));
}

// norma infinita
double norm_inf(double* vet, int len) {
  int i;
  double maior=0;
  for(i=0;i<len;i++)
    if(fabs(vet[i])>maior)
      maior=fabs(vet[i]);
  return maior;
}

// produto vetor-escalar
double* scavec(double escalar, double* vet, int len) {
  int i;
  double* vet2 = (double *)malloc(len * sizeof(double));
  for(i=0;i<len;i++)
    vet2[i]=escalar*vet[i];
  return vet2;
}

// retorna uma cópia de um array
double* copia_array(double* vet1, int len){
  int i;
  double* vet2 = (double *)malloc(len * sizeof(double));
  for(i=0;i<len;i++)
    vet2[i]=vet1[i];
  return vet2;
}

/*  Copia uma array para dentro de uma matriz
    Dada uma matriz mat, um parametro pos e um parametro len,
    mat[pos][i] <- vet[i] para todo i | 0<=i<len
*/
void copia_array_para_matriz(double* vet, int pos, int len, double** mat){
  int i;
  for (i=0;i<len;i++)
    mat[pos][i] = vet[i];
}

double* residuo(int n, double* b, double* x){
	double* r = (double *)malloc(n*sizeof(double));
	int i;
	r = matvec(n, x);
	for (i = 0; i < n; i++)
		r[i] = b[i] - r[i];
	
	return r;
}

double* gmres(int lmax, double tol, int k, int *l, FILE* out){
	
	int width, height, n, nz, _x, _y;
	
	// tamanho horizontal da malha
	width	= (int)(_b-_a)/hx + 1.5;
	// tamanho vertical da malha
	height	= (int)(_d-_c)/hy + 1.5;
	// ordem da matriz
	n	= (width)*(height);
	// numero de elementos nao nulos (antes da aplicacao das condicoes de contorno)
	nz	= 5*n-2*width-2;

  // se k = 0, nao usa restart
  if(k==0)
    k=n;

  // declaracoes
  int i, j, t;
  double ji;
  double** u = (double **)malloc((k+1)*sizeof(double*));
  for(i=0;i<=k;i++)
    u[i] = (double *)malloc(n*sizeof(double));
  double* e = (double *)malloc(k * sizeof(double));
  double* x = (double *)calloc(n, sizeof(double));
  double** h = (double **)malloc(k*sizeof(double*));
  for(i=0;i<k;i++)
    h[i] = (double *)malloc((k+1)*sizeof(double));
  double p;
  double* c = (double *)calloc(k, sizeof(double));
  double* s = (double *)calloc(k, sizeof(double));
  double* r = (double *)calloc(k, sizeof(double));
  double* y = (double *)calloc(k, sizeof(double));
  double soma_hy;
  
  double* aux0;
  double * res;
  double norma_r;
  
	/////// GERA VETOR B ////////
	
	double* b = (double*)calloc(n,sizeof(double));
	
	for(j=0; j < n; j++) {
		
		_x = j%width;
		_y = j/width;
		
		// condicao de valor prescrito
		if ((_x == 0) && (pvc_b->tipo == 1))
			b[j] = pvc_b->u;
		else if ((_x == width-1) && (pvc_c->tipo == 1))
			b[j] = pvc_c->u;
		else if ((_y == 0) && (pvc_d->tipo == 1))
			b[j] = pvc_d->u;
		else if ((_y == height-1) && (pvc_e->tipo == 1))
			b[j] = pvc_e->u;
		// condicao de fluxo prescrito
		else if ((_x == 0) && (pvc_b->tipo == 2))
			b[j] = gera_f(_x,_y,hx,hy) + (-_k/(hx*hx) - bx(_x,_y,hx,hy)/(2.0*hx))*hx*pvc_b->sigma/_k;
		else if ((_x == width-1) && (pvc_c->tipo == 2))
			b[j] = gera_f(_x,_y,hx,hy) + (-_k/(hx*hx) + bx(_x,_y,hx,hy)/(2.0*hx))*hx*pvc_c->sigma/_k;
		else if ((_y == 0) && (pvc_d->tipo == 2))
			b[j] = gera_f(_x,_y,hx,hy) + (-_k/(hy*hy) - by(_x,_y,hx,hy)/(2.0*hy))*hy*pvc_d->sigma/_k;
		else if ((_y == height-1) && (pvc_e->tipo == 2))
			b[j] = gera_f(_x,_y,hx,hy) + (-_k/(hy*hy) + by(_x,_y,hx,hy)/(2.0*hy))*hy*pvc_e->sigma/_k;
		// condicao mista
		else if ((_x == 0) && (pvc_b->tipo == 3))
			b[j] = gera_f(_x,_y,hx,hy) - (-_k/(hx*hx) - bx(_x,_y,hx,hy)/(2.0*hx))*hx*pvc_b->gamma/pvc_b->alfa;
		else if ((_x == width-1) && (pvc_c->tipo == 3))
			b[j] = gera_f(_x,_y,hx,hy) - (-_k/(hx*hx) + bx(_x,_y,hx,hy)/(2.0*hx))*hx*pvc_c->gamma/pvc_c->alfa;
		else if ((_y == 0) && (pvc_d->tipo == 3))
			b[j] = gera_f(_x,_y,hx,hy) - (-_k/(hy*hy) - by(_x,_y,hx,hy)/(2.0*hy))*hy*pvc_d->gamma/pvc_d->alfa;
		else if ((_y == height-1) && (pvc_e->tipo == 3))
			b[j] = gera_f(_x,_y,hx,hy) - (-_k/(hy*hy) + by(_x,_y,hx,hy)/(2.0*hy))*hy*pvc_e->gamma/pvc_e->alfa;
		// centro da malha
		else
			b[j] = gera_f(_x,_y,hx,hy);
	}
	
	///////////// fim da geracao do vetor de termos independentes

  //e = tol||b||2
  double _e = tol*norm_2(b,n);
  *l = 0;

  do { // repeat

    i = 0;

    // u[i] = b - Ax
    aux0 = residuo(n,b,x);
    for(t=0; t<n; t++)
      u[i][t] = aux0[t];

    // e[i] = ||u[i]||2 = p
    e[i] = p = norm_2(&u[i][0],n);

    // u[i]=r/e[i]
    for(t=0; t<n; t++)
      u[i][t] = aux0[t]/e[i];
    free(aux0);

    do{

      // Gram-Schmidt

      aux0 = matvec(n,&u[i][0]);
      copia_array_para_matriz(aux0, i+1, n, u);
      free(aux0);
      
      for(j=0;j<=i;j++){

        h[i][j] = vecvec(&u[i+1][0],&u[j][0],n);

        for(t=0; t<n; t++)
          u[i+1][t] = u[i+1][t] - h[i][j]*u[j][t];
        /*copia_array_para_matriz(
          vec_plus_vec(&u[i+1][0],scavec(-1*h[i][j],&u[j][0],n),n),
          i+1, n, u);*/
      }

      h[i][i+1]=norm_2(&u[i+1][0],n);

      for(t=0; t<n; t++)
        u[i+1][t] = u[i+1][t]/h[i][i+1];
      /*copia_array_para_matriz(
        scavec(1/h[i][i+1],&u[i+1][0],n)
        ,i+1,n,u);*/

      // Algoritmo QR
      for(j=0;j<i;j++){
        ji = h[i][j];
        h[i][j] = c[j]*ji + s[j]*h[i][j+1];
        h[i][j+1] = -s[j]*ji + c[j]*h[i][j+1];
      }
    
      r[i] = sqrt(h[i][i]*h[i][i] + h[i][i+1]*h[i][i+1]);
      c[i] = h[i][i]/r[i];
      s[i] = h[i][i+1]/r[i];
      h[i][i] = r[i];
      h[i][i+1] = 0;
      e[i+1] = -s[i]*e[i];
      e[i] = c[i]*e[i];
      p = fabs(e[i+1]);
      // i = i + 1
      i++;
    }while((p>_e) && (i<k));
    i = i - 1;
	  y[i] = e[i]/h[i][i];
	  for (t=i-1;t>=0;t--)
	  {
	  	soma_hy = 0;
	  	for (j=t+1;j<=i;j++)
	  		soma_hy += h[j][t]*y[j];	
	  	y[t] = (1./h[t][t])*(e[t]-soma_hy);
	  }	
    // x = sum(y[j]u[j]) | 1<=j<=i
    for(j=0;j<=i;j++){
      //copia_array_para_matriz(
      for(t=0; t<n; t++)
        x[t] += u[j][t]*y[j];
      //x = vec_plus_vec(scavec(y[j],&u[j][0],n),x,n);
        //,j, k, n, u);
    }
    (*l)++;
  } while((p>=_e) && (*l<lmax)); // until
  res = residuo(n,b,x);
  norma_r = norm_inf(res,n);
  free(res);
  printf("Residuo: %f\n", norma_r);
  
  //free(e);
  free(c);
  free(s);
  free(r);
  free(y);
  for(i=0;i<=k;i++)
    free(u[i]);
  //free(u);
  for(i=0;i<k;i++)
    free(h[i]);
  //free(h);
  
  return x;
}